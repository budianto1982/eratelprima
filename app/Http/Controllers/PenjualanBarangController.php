<?php

namespace App\Http\Controllers;

use App\barang;
use App\Penjualan;
use App\PenjualanDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PenjualanBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barangs = barang::where('stok', '>', 0)->orderBy('nama_barang')->pluck('nama_barang', 'id')->all();
        return view('penjualan-barang.index', compact('barangs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tambahBarang(Request $request)
    {
        $barangId = $request->barang_id;
        $barang = barang::find($barangId);
        $defaultJumlah = 1;

        return view('penjualan-barang._tambah-barang', compact('barang', 'defaultJumlah'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $httpCode = 200;
        $message = "Data penjualan berhasil disimpan";

        if (isset($request->barang_ids, $request->kd_barangs, $request->nama_barangs)) {
            try {
                $kdPenjualan = $this->getLatestKodePenjualan();
                $tglPenjualan = date('Y-m-d');
                // $total = array_sum($request->sub_totals);
                $total = $request->total;
                $total = ($total != '' && $total != 0) ? $total : array_sum($request->sub_totals);
                $bayar = $request->bayar;
                $kembalian = $request->kembalian;
                $kembalian = ($kembalian != '' && $kembalian != 0) ? $kembalian : $bayar - $total;

                // Create Penjualan
                DB::beginTransaction();
                $penjualan = new Penjualan;
                $penjualan->kd_penjualan = $kdPenjualan;
                $penjualan->tgl_penjualan = $tglPenjualan;
                $penjualan->total = $total;
                $penjualan->bayar = $bayar;
                $penjualan->kembalian = $kembalian;
                $penjualan->user_id = \Auth::user()->id;
                
                // Save Penjualan
                if ($penjualan->save()) {
                    // Create Penjualan Detail
                    $penjualanDetails = [];
                    $barangIds = $request->barang_ids;
                    $kodeBarangs = $request->kd_barangs;
                    $namaBarangs = $request->nama_barangs;
                    $hargaBarangs = $request->harga_juals;
                    $stoks = $request->stoks;
                    $jumlahs = $request->jumlahs;
                    $subTotals = $request->sub_totals;
                    for ($i=0; $i < count($barangIds); $i++) {
                        $penjualanDetail = new PenjualanDetail;
                        $penjualanDetail->penjualan_id = $penjualan->id;
                        $penjualanDetail->barang_id = $barangIds[$i];
                        $penjualanDetail->kd_barang = $kodeBarangs[$i];
                        $penjualanDetail->nm_barang = $namaBarangs[$i];
                        $penjualanDetail->harga_jual = $hargaBarangs[$i];
                        $penjualanDetail->jumlah = $jumlahs[$i];
                        $penjualanDetail->sub_total = $subTotals[$i];
                        if ($penjualanDetail->save()) {
                            $barang = barang::find($penjualanDetail->barang_id);
                            $stok = $barang->stok;
                            $jumlahDiBeli = $penjualanDetail->jumlah;
                            $sisaStok = $stok - $jumlahDiBeli;
                            // Update stok barang
                            $barang->stok = $sisaStok;
                            $barang->save();
                        }

                        // Bulk Insert
                        // array_push($penjualanDetails, [
                        //     'penjualan_id' => $penjualan->id,
                        //     'barang_id' => $barangIds[$i],
                        //     'kd_barang' => $kodeBarangs[$i],
                        //     'nama_barang' => $namaBarangs[$i],
                        //     'harga_barang' => $hargaBarangs[$i],
                        //     'jumlah' => $jumlahs[$i],
                        //     'sub_total' => $subTotals[$i],
                        //     'created_at' => date('Y-m-d H:i:s'),
                        //     'updated_at' => date('Y-m-d H:i:s')
                        // ]);
                    }
                    // Save Penjualan Detail Bulk Insert
                    // PenjualanDetail::create($modelData);
                    DB::commit();
                }
            } catch (Exception $e) {
                DB::rollback();
                $httpCode = $e->getCode();
                $message = $e->getMessage();
            }
        } else {
            $httpCode = 404;
            $message = "Mohon untuk menginput data barang yang akan dijual terlebih dahulu";
        }

        // return response()->json([
        //     'message' => $message
        // ], $httpCode);
        return redirect()->route('penjualan-barang.index')->with('flash_notification_message', $message);
    }

    private function getLatestKodePenjualan() {
        $latestKodePenjualan = "PEN";
        $penjualanExists = Penjualan::whereNotNull('kd_penjualan')->count();
        if ($penjualanExists > 0) {
            $latestKodePenjualan .= collect(DB::select("SELECT * FROM v_next_kd_penjualan"))->first()->kd_penjualan;
        } else {
            $latestKodePenjualan .= "00001";
        }

        return $latestKodePenjualan;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
