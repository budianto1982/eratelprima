@extends('barang.base')
@section('action-content')
    <!-- Main content -->
<section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-10">
          <h3 class="box-title">List  User</h3><br><br><br>
          <a href="{{ action('UserController@create') }}" class="btn btn-success btn-sm mb-2">
            <i class="fa fa-plus-circle"></i> Tambah User Baru
        </a>
        </div>
        <div>
        
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
     
    <div id="example2_wrapper" class="table table-bordered table-striped">
      <div class="row">
        <div class="col-sm-12 col-xs-5 ">
        <table id="userDtab" class="table table-xs table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                <th>Action</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>
            </thead>
            <tbody>
                    @forelse ($users as $user)
                    <tr>
                        <td class="align-middle">
                            <span>
                                <a href="{{ route('user.edit', ['id' => $user->id]) }}" class="btn btn-outline-primary btn-sm">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a onclick="return confirm('Are you sure?')" href="{{ route('user.delete', ['id' => $user->id]) }}" class="btn btn-outline-danger btn-sm">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </span>
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role_name }}</td>
                      
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" class="text-center text-muted">Tidak ada data</td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">
        </div>
        <div class="col-sm-7">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
        
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
</div>
    </section>
    <!-- /.content -->
  </div>
@endsection