@extends('layouts.app-base')

@section('content-header')
<h1>Master Barang</h1>
@stop

@section('content')

<!-- Main content -->
<div class="box">
    <div class="box-header">
        <div class="row">
            <div class="col-sm-10">
                <h3 class="box-title"></h3>
            </div>
            <div class="col-sm-2" style="text-align: right">
                <a href="{{ route('barang.create') }}" class="btn btn-success btn-sm mb-2">
                    <i class="fa fa-plus-circle"></i> Tambah Data
                </a>
            </div>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="barangDtab" class="table table-xs table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Kode Supplier</th>
                    <th>Nama Supplier</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Satuan</th>
                    <th>Harga Jual</th>
                    <th>Harga Beli</th>
                    <th>Stok</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($barangs as $barang)
                <tr>
                    <td>{{ $barang->kd_supplier}}</td>
                    <td>{{ $barang->nama_supplier}}</td>
                    <td>{{ $barang->kd_barang}}</td>
                    <td>{{ $barang->nama_barang}}</td>
                    <td>{{ $barang->satuan }}</td>
                    <td>{{ $barang->harga_jual }}</td>
                    <td>{{ $barang->harga_beli }}</td>
                    <td>{{ $barang->stok }}</td>
                    <td style="text-align: center">
                        <span>
                            <a href="{{ route('barang.edit', ['id' => $barang->id]) }}"
                                class="btn btn-outline-primary btn-xs">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a onclick="return confirm('Are you sure?')"
                                href="{{ route('barang.delete', ['id' => $barang->id]) }}"
                                class="btn btn-outline-danger btn-xs">
                                <i class="fa fa-trash"></i>
                            </a>
                        </span>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="7" class="text-center text-muted">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.content -->

<script>
    $(document).ready(function () {
        $('#barangDtab').DataTable({
            bLengthChange: false,
            order: [
                [0, 'asc']
            ],
            responsive: true
        });
    });
</script>

@endsection
