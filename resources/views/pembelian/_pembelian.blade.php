<tr id="{{ $barang->id }}-item-dtab-tr">
    <td>
        {!! Form::hidden('barang_ids[]', $barang->id) !!}
        {!! Form::text('kd_barangs[]', $barang->kd_barang, ['class' => 'form-control', 'readonly']) !!}
    </td>
    <td>{!! Form::text('nama_barangs[]', $barang->nama_barang, ['class' => 'form-control', 'readonly']) !!}</td>
    <td>{!! Form::text('harga_belis[]', $barang->harga_beli, ['class' => 'form-control harga-textbox', 'id' => $barang->id .'-harga-textbox', 'readonly']) !!}</td>
    <td>{!! Form::text('stoks[]', $barang->stok, ['class' => 'form-control', 'readonly']) !!}</td>
    <td>{!! Form::text('jumlahs[]', $defaultJumlah, ['class' => 'form-control jumlah-textbox', 'id' => $barang->id .'-jumlah-textbox', 'data-barang-id' => $barang->id]) !!}</td>
    <td>{!! Form::text('sub_totals[]', $barang->harga_jual * $defaultJumlah, ['class' => 'form-control sub-total-textbox', 'id' => $barang->id .'-sub-total-textbox', 'readonly']) !!}</td>
    <td class="align-middle">
        <span>
            <a data-barang-id="{{ $barang->id }}" class="btn btn-outline-danger btn-sm remove-item-btn">
                <i class="fa fa-trash"></i>
            </a>
        </span>
    </td>
</tr>