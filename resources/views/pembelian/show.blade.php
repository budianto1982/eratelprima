@extends('layouts.app-base')

@section('content-header')
<h1>Pembelian Barang [{{ $pembelian->kd_pembelian }}]<small>Detail</small></h1>
@stop

@section('content')

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">
            {{ $pembelian->tgl_pembelian }} <strong> | {{ $pembelian->Supplier->nama_supplier }}</strong>
        </h3>
        <div class="box-tools">
            <span class="label label-{{ $pembelian->status_color }}">{{ $pembelian->status }}</span>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- .box-body start -->
    <div class="box-body">
        <table id="pembelianDtab" class="table table-bordered table-striped dataTable" style="width:100%">
            <thead>
                <tr>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Harga</th>
                    <th>Jumlah</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pembelianDetails as $pembelianDetail)
                <tr>
                    <td>{{ $pembelianDetail->kd_barang}}</td>
                    <td>{{ $pembelianDetail->nm_barang}}</td>
                    <td>{{ $pembelianDetail->harga_beli }}</td>
                    <td>{{ $pembelianDetail->jumlah }}</td>
                    <td>{{ $pembelianDetail->sub_total }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" class="text-center text-muted">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" style="text-align: right"><strong>Total</strong></td>
                    <td><strong>{{ $pembelian->total_pembelian }}</strong></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        @if ($showTerimaButton)
            <a href="{{ route('penerimaan.terima', ['id' => $pembelian->id]) }}" class="btn btn-success pull-right">
                <i class="fa fa-check-square-o"></i> Terima Barang
            </a>
            <a href="{{ route('penerimaan.index') }}" class="btn btn-default pull-right">Back</a>
        @else
            <a href="{{ route('pembelian.index') }}" class="btn btn-default pull-right">Back</a>
        @endif
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#pembelianDtab').DataTable({
            bLengthChange: false,
            fixedHeader: {
                header: true,
                footer: true
            },
            order: [
                [2, 'asc'],
                [1, 'asc']
            ],
            responsive: true
        });
    });
</script>

@endsection
