@extends('layouts.app-base')

@section('content-header')
<h1>Pembelian Barang</h1>
@stop

@section('content')

<!-- Main content -->
<div class="box">
    <div class="box-header">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="box-title"></h3>
            </div>
            <div class="col-sm-6" style="text-align: right">
                {!! Form::open(['route' => 'pembelian.create', 'method' => 'get']) !!}
                <div class="input-group">
                    {!! Form::select('supplier_id', $suppliers, null, ['class' => 'select', 'id' => 'supplier-dropdown', 'placeholder' => 'Pilih suppllier untuk tambah data']) !!}
                    <span class="input-group-btn">
                        <button class="btn btn-success btn-flat">
                            <i class="fa fa-plus-circle"></i> Tambah Data
                        </button>
                    </span>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="pembelianDtab" class="table table-bordered table-striped dataTable" style="width:100%">
            <thead>
                <tr>
                    <th>Kode Pembelian</th>
                    <th>Tanggal Pembelian</th>
                    <th>Supplier</th>
                    <th>Total Pembelian</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pembelians as $pembelian)
                <tr>
                    <td>{{ $pembelian->kd_pembelian}}</td>
                    <td>{{ $pembelian->tgl_pembelian}}</td>
                    <td>{{ $pembelian->Supplier->nama_supplier }}</td>
                    <td>{{ $pembelian->total_pembelian }}</td>
                    <td>{{ $pembelian->status }}</td>
                    <td style="text-align: center">
                        <span>
                            <a href="{{ route('pembelian.show', ['id' => $pembelian->id]) }}"
                                class="btn btn-outline-primary btn-xs">
                                <i class="fa fa-eye"></i>
                            </a>
                            @if ($pembelian->status != "diterima")
                              <a onclick="return confirm('Are you sure?')"
                                  href="{{ route('pembelian.delete', ['id' => $pembelian->id]) }}"
                                  class="btn btn-outline-danger btn-xs">
                                  <i class="fa fa-trash"></i>
                              </a>
                            @endif
                        </span>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" class="text-center text-muted">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.content -->

<script>
    $(document).ready(function () {
        $('#pembelianDtab').DataTable({
            bLengthChange: false,
            order: [
                [1, 'asc'],
                [0, 'asc']
            ],
            responsive: true
        });
    });
</script>

@endsection
