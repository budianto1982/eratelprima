<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenjualanDetail extends Model
{
    protected $table = 'penjualan_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'penjualan_id',
        'barang_id',
        'kd_barang',
        'nm_barang',
        'harga_jual',
        'jumlah',
        'sub_total'
    ];
}
