@extends('layouts.app-base')

@section('content-header')
<h1>Master Barang <small>Tambah Baru</small></h1>
@stop

@section('content')

<!-- Main content -->
<div class="box">
    <div class="box-body">
        {!! Form::open(['route' => 'barang.store', 'style' => 'padding:15px']) !!}
        @include('barang._barang', ['submitLabel' => 'Save'])
        {!! Form::close() !!}
    </div>
</div>

@stop
