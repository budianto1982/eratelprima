<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembelianDetail extends Model
{
    protected $table = 'pembelian_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pembelian_id',
        'barang_id',
        'kd_barang',
        'nm_barang',
        'harga_beli',
        'jumlah',
        'sub_total'
    ];
}
