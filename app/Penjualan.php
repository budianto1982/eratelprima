<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    protected $table = 'penjualan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kd_penjualan',
        'tgl_penjualan',
        'total',
        'bayar',
        'kembalian',
        'user_id'
    ];

    public function PenjualanDetails() {
        return $this->hasMany('App\PenjualanDetail');
    }
}
