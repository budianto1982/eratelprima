@extends('user-mgmt.base')
@section('action-content')
    <!-- Main content -->
    
    <div class="panel panel-default">
    <div class="panel-body">
        <div>
        <!-- <a href="{{ action('UserController@create') }}" class="btn btn-info btn-sm mb-2"> -->
        <a href="{{ route('user.create') }}" class="btn btn-info btn-sm mb-2">
            <i class="fa fa-plus-circle"></i> Tambah User
        </a>
        </div>
        <br><br>
        <table id="userDtab" class="table table-xs table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($users as $user)
                    <tr>
                        <td class="align-middle">
                            <span>
                                <a href="{{ route('user.edit', ['id' => $user->id]) }}" class="btn btn-outline-primary btn-sm">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a onclick="return confirm('Are you sure?')" href="{{ route('user.delete', ['id' => $user->id]) }}" class="btn btn-outline-danger btn-sm">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </span>
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role_name }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center text-muted">Tidak ada data</td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                    </tr>
                @endforelse
            </tbody>
        </table>

    </div>
</div>

<script>
</script>

@endsection