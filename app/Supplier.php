<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Supplier';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kd_supplier',
        'nama_supplier',
        'alamat',
        'no_telp',
        'email'
    ];
}
