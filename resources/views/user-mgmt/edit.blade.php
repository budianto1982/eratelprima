@extends('user-mgmt.base')
@section('action-content')

<div class="panel panel-primary">
    <div class="panel-heading">Edit User</div>
    <div class="panel-body">

    {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'PATCH']) !!}

        @include('user-mgmt._user', ['roles' => $roles, 'submitLabel' => 'Update'])

    {!! Form::close() !!}

    
    </div>
</div>

@stop