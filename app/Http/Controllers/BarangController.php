<?php

namespace App\Http\Controllers;
use App\barang;
use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use DB;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $barangs = barang::join('supplier', 'barang.supplier_id', '=', 'supplier.id')
            ->select('barang.*', 'kd_supplier', 'nama_supplier')
            ->orderBy('supplier.nama_supplier')
            ->orderBy('barang.nama_barang')
            ->get();

        return view('barang.index', compact('barangs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::orderBy('nama_supplier')->pluck('nama_supplier', 'id')->all();
        $barangs = new Barang;

        return view('barang.create', compact('barangs','suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kdBarang = $this->getLatestKode();
        $barang = new Barang;
        $barang->kd_barang = $kdBarang;
        $barang->nama_barang = $request->nama_barang;
        $barang->satuan = $request->satuan;
        $barang->harga_jual = $request->harga_jual;
        $barang->harga_beli = $request->harga_beli;
        $barang->stok = $request->stok;
        $barang->supplier_id = $request->supplier_id;
        $barang->save();

        return redirect()->route('barang.index')->with('flash_notification_message', 'Barang berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $kd_barang
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $kd_barang
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suppliers = Supplier::orderBy('nama_supplier')->pluck('nama_supplier', 'id')->all();
        $barang = Barang::find($id);

        return view('barang.edit', compact('barang', 'suppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $kd_barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $barang = Barang::find($id);
        $barang->fill($data);
        $barang->save();

        return redirect()->route('barang.index')->with('flash_notification_message', 'Barang berhasil diubah');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $kd_barang
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = Barang::where('id', $id)->delete();

        return redirect()->route('barang.index')->with('flash_notification_message', 'Barang berhasil dihapus');
    }

    private function getLatestKode() {
        $latestKode = "EP";
        $barangExists = barang::whereNotNull('kd_barang')->count();
        if ($barangExists > 0) {
            $latestKode .= 
                collect(
                    DB::select(
                        "SELECT LPAD(CAST(RIGHT(kd_barang, 4) AS UNSIGNED) + 1, 4, '0') AS kd_barang FROM barang ORDER BY CAST(RIGHT(kd_barang, 4) AS UNSIGNED) DESC LIMIT 1"
                    )
                )->first()->kd_barang;
        } else {
            $latestKode .= "0001";
        }

        return $latestKode;
    }
}
