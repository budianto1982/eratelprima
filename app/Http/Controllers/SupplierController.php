<?php

namespace App\Http\Controllers;
use App\Supplier;
use Illuminate\Http\Request;
use DB;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         $suppliers = Supplier::paginate(10);
        //  return $barangs;
         return view('supplier.index', compact('suppliers'));
  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $supplier = new Supplier;

        return view('supplier.create', compact('supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplier = new Supplier;
        $kdSupplier = $this->getLatestKode();
        $data = $request->all();
        $data['kd_supplier'] = $kdSupplier;
        $supplier->fill($data);
        $supplier->save();

        return redirect()->route('supplier.index')->with('flash_notification_message', 'Supplier berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::find($id);

        return view('supplier.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);
        $data = $request->all();
        $supplier->update($data);

        return redirect()->route('supplier.index')->with('flash_notification_message', 'Supplier berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::where('id', $id)->delete();

        return redirect()->route('supplier.index')->with('flash_notification_message', 'Supplier berhasil dihapus');

    }

    private function getLatestKode() {
        $latestKode = "SE";
        $rowExists = Supplier::whereNotNull('kd_supplier')->count();
        if ($rowExists > 0) {
            $latestKode .= 
                collect(
                    DB::select(
                        "SELECT LPAD(CAST(RIGHT(kd_supplier, 4) AS UNSIGNED) + 1, 4, '0') AS kd_supplier FROM supplier ORDER BY CAST(RIGHT(kd_supplier, 4) AS UNSIGNED) DESC LIMIT 1"
                    )
                )->first()->kd_supplier;
        } else {
            $latestKode .= "0001";
        }

        return $latestKode;
    }
}
