@extends('layouts.app-base')

@section('content-header')
<h1>Master Supplier <small>Tambah Baru</small></h1>
@stop

@section('content')

<!-- Main content -->
<div class="box">
    <div class="box-body">
        {!! Form::model($supplier, ['route' => 'supplier.store', 'style' => 'padding:15px']) !!}
        @include('supplier._supplier', ['submitLabel' => 'Save'])
        {!! Form::close() !!}
    </div>
</div>

@stop
