<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class barang extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'barang';
    protected $fillable = ['kd_barang','nama_barang','satuan','harga_jual','harga_beli','stok','supplier_id'];
    
    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = [];

    public function Supplier() {
        return $this->belongsTo('App\Supplier');
    }
}
