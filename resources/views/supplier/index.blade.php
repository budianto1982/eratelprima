@extends('layouts.app-base')

@section('content-header')
<h1>Master Supplier</h1>
@stop

@section('content')

<!-- Main content -->
<div class="box">
    <div class="box-header">
        <div class="row">
            <div class="col-sm-10">
                <h3 class="box-title"></h3>
            </div>
            <div class="col-sm-2" style="text-align: right">
                <a href="{{ route('supplier.create') }}" class="btn btn-success btn-sm mb-2">
                    <i class="fa fa-plus-circle"></i> Tambah Data
                </a>
            </div>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table id="supplierDtab" class="table table-xs table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Kode Supplier</th>
                    <th>Nama Supplier</th>
                    <th>Alamat</th>
                    <th>No. Telp</th>
                    <th>Email</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($suppliers as $supplier)
                <tr>
                    <td>{{ $supplier->kd_supplier}}</td>
                    <td>{{ $supplier->nama_supplier}}</td>
                    <td>{{ $supplier->alamat }}</td>
                    <td>{{ $supplier->no_telp }}</td>
                    <td>{{ $supplier->email }}</td>
                    <td style="text-align: center">
                        <span>
                            <a href="{{ route('supplier.edit', ['id' => $supplier->id]) }}"
                                class="btn btn-outline-primary btn-xs">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a onclick="return confirm('Are you sure?')"
                                href="{{ route('supplier.delete', ['id' => $supplier->id]) }}"
                                class="btn btn-outline-danger btn-xs">
                                <i class="fa fa-trash"></i>
                            </a>
                        </span>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" class="text-center text-muted">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.content -->

<script>
    $(document).ready(function () {
        $('#supplierDtab').DataTable({
            bLengthChange: false,
            order: [
                [0, 'asc'],
                [1, 'asc']
            ],
            responsive: true
        });
    });
</script>

@endsection
