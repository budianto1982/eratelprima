<?php

Auth::routes();

Route::group(['middleware' =>['auth']],function(){
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    
    Route::group(['middleware' => ['gudang']], function() {
        Route::get('penerimaan', 'PembelianController@penerimaan')->name('penerimaan.index');
        Route::get('penerimaan/{pembelian}', 'PembelianController@show')->name('penerimaan.show');
        Route::get('penerimaan/{pembelian}/terima', 'PembelianController@terima')->name('penerimaan.terima');
    });

    Route::group(['middleware' => ['admin']], function() {
        // User
        Route::resource('user', 'UserController');
        // Barang
        Route::get('barang/{barang}/delete', 'BarangController@destroy')->name('barang.delete');
        Route::resource('barang', 'BarangController');
        // Supplier
        Route::get('supplier/{supplier}/delete', 'SupplierController@destroy')->name('supplier.delete');
        Route::resource('supplier', 'SupplierController');
        // Pembelian Barang
        Route::get('pembelian/tambah-barang', 'PembelianController@tambahBarang')->name('pembelian.tambah-barang');
        Route::get('pembelian/{pembelian}/delete', 'PembelianController@destroy')->name('pembelian.delete');
        Route::resource('pembelian', 'PembelianController');
    });

    Route::group(['middleware' => ['kasir']], function() {
        Route::resource('stock-barang', 'StockBarangController');
        Route::get('penjualan-barang/tambah-barang', 'PenjualanBarangController@tambahBarang')->name('penjualan-barang.tambah-barang');
        Route::resource('penjualan-barang', 'PenjualanBarangController');
    });
});
