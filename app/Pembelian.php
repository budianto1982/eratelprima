<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
      /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Pembelian';

    /**
    * The accessors to append to the model's array form.
    *
    * @var array
    */
    protected $appends = [
        'status_color'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kd_pembelian',
        'tgl_pembelian',
        'supplier_id',
        'total_pembelian',
        'status',
        'pembuat_user_id',
        'tgl_penerimaan',
        'penerima_user_id'        
    ];

    public function getStatusColorAttribute() {
        $status = \strtoupper($this->status);
        $color = "default";

        if ($status == "DITERIMA") {
            $color = "success";
        } else if ($status == "DIPESAN") {
            $color = "warning";
        } else if ($status == "BATAL") {
            $color = "danger";
        }

        return $color;
    }

    public function Supplier() {
        return $this->belongsTo('App\Supplier');
    }

    public function PembelianDetails() {
        return $this->hasMany('App\PembelianDetail');
    }
}
