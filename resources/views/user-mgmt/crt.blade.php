@extends('user-mgmt.base')
@section('action-content')
    <!-- Main content -->
    <section>
    <div class="panel panel-default">
    <div class="panel-heading">User</div>
    <div class="panel-body">

       
        <table id="userDtab" class="table table-xs table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>
            </thead>
            <tbody>
               
                    <tr>
                       
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="text-center text-muted">Tidak ada data</td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                        <td style="display: none;"></td>
                    </tr>
        
            </tbody>
        </table>

    </div>
</div>

</section>

@endsection