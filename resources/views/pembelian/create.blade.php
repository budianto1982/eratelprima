@extends('layouts.app-base')

@section('content-header')
<h1>Pembelian Barang<small>Tambah Baru</small></h1>
@stop

@section('content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">
            {{ $supplier->kd_supplier }} <strong> | {{ $supplier->nama_supplier }}</strong>
        </h3>
    </div>
    <div class="box-body">
        <div style="margin-bottom: 15px">
            <div class="input-group">
                {!! Form::select('barang-dropdown', $barangs, null, ['class' => 'select', 'id' => 'barang-dropdown', 'placeholder' => 'Cari barang']) !!}
                <span class="input-group-btn">
                    <button type="button" class="btn btn-success btn-flat" id="submit-btn" disabled>Simpan</button>
                </span>
            </div>
        </div>
        {!! Form::open(['route' => 'pembelian.store', 'id' => 'pembelian-barang-form']) !!}
        <table id="item-dtab" class="table table-bordered table-striped dataTable" style="width:100%">
            <thead>
                <tr>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Jumlah</th>
                    <th>Sub Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <td colspan="5" style="text-align:right"><strong>Total</strong></td>
                    <td>{!! Form::text('total_pembelian', 0, ['class' => 'form-control', 'id' => 'total-textbox', 'readonly']) !!}</td>
                    <td>
                        {!! Form::hidden('supplier_id', $supplier->id, ['id' => 'supplier_id']) !!}
                    </td>
                </tr>
            </tfoot>
        </table>
        {!! Form::close() !!}
    </div>
</div>
<!-- /.content -->

<script>
    $(document).ready(function() {
        $('#barang-dropdown').on('select2:select', function (e) {
            var data = e.params.data;
            barangId = data.id;
            barangName = data.text;
            addItem(barangId);
        });

        $('#submit-btn').on('click', function() {
            $('#pembelian-barang-form').submit();
        });

        function addItem(barangId) {
            $.ajax({
                url: "{{ route('pembelian.tambah-barang') }}",
                type: 'GET',
                dataType: 'HTML',
                data: {
                    'barang_id': barangId
                },
                beforeSend: function() {
                    $("#barang-dropdown").prop("disabled", true);
                },
                success: function(result) {
                    $("#item-dtab tbody").append(result);
                    $('input.jumlah-textbox').keyup(function() {
                        barangId = $(this).data('barang-id');
                        jumlah = ($(this).val()) * 1;
                        calculateSubTotal(barangId, jumlah);
                    });
                    $('a.remove-item-btn').click(function() {
                        barangId = $(this).data('barang-id');
                        removeItem(barangId);
                    });
                    // Disabling selected barang option from dropdown
                    $('#barang-dropdown option[value="'+ barangId +'"]').prop('disabled', true);
                    $('.select').select2({
                        selectOnClose: true,
                        width: '100%'
                    });
                    calculateTotal();
                },
                error: function(result, status, xhr) {
                    alert(result.responseJSON.message);
                },
                complete: function() {
                    $('#barang-dropdown').val(null).trigger('change');
                    $("#barang-dropdown").prop("disabled", false);
                    checkBarangNotEmpty();
                }
            });
        }

        function removeItem(barangId) {
            $("#item-dtab tbody").find("tr#"+ barangId +"-item-dtab-tr").remove();
            // Enabling disabled barang option from dropdown
            $('#barang-dropdown option[value="'+ barangId +'"]').prop('disabled', false);
            $('.select').select2({
                selectOnClose: true,
                width: '100%'
            });
            calculateTotal();
            checkBarangNotEmpty();
        }

        function calculateSubTotal(barangId, jumlah) {
            jumlah = (jumlah == '') ? 0 : jumlah;
            harga = ($('#'+ barangId +'-harga-textbox').val()) * 1;
            subTotalBarang = harga * jumlah;
            $('#'+ barangId +'-sub-total-textbox').val(subTotalBarang);
            calculateTotal();
        }

        function calculateTotal() {
            total = 0;
            $('input.sub-total-textbox').each(function( index ) {
                subTotal = ($(this).val()) * 1;
                total = total + subTotal;
            });
            $('#total-textbox').val(total);
        }

        function checkBarangNotEmpty() {
            if (($('#supplier_id').val() != '') && ($('#item-dtab tbody tr').length)) {
                $('#submit-btn').prop('disabled', false);
            } else {
                $('#submit-btn').prop('disabled', true);
            }
        }
    });
</script>

@endsection
