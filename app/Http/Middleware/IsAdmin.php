<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role_id != 1 ){
            return redirect()->route('dashboard');
        }
        return $next($request);
    }
    // public function handle($request, Closure $next) {
    //     if (!Auth::onceBasic()) {
    //          if ($request->ajax() || $request->wantsJson()) {
    //              return response('Unauthorized.', 401);
    //          } else {
    //              return redirect()->guest('login');
    //          }
    //      }
     
    //      return $next($request);
    //  }
}
