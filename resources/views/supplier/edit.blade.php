@extends('layouts.app-base')

@section('content-header')
<h1>Master Supplier <small>Ubah Data</small></h1>
@stop

@section('content')

<!-- Main content -->
<div class="box">
    <div class="box-body">
        {!! Form::model($supplier, ['route' => ['supplier.update', $supplier->id], 'method' => 'PATCH', 'style' => 'padding:15px']) !!}
        @include('supplier._supplier', ['submitLabel' => 'Update'])
        {!! Form::close() !!}
    </div>
</div>

@stop