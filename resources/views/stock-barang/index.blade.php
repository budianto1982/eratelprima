@extends('layouts.app-base')

@section('content-header')
<h1>Stok Barang</h1>
@stop

@section('content')

<!-- Main content -->
<div class="box">
    <div class="box-body">
        <table id="stokBarangDtab" class="table table-bordered table-striped dataTable" style="width:100%">
            <thead>
                <tr>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Satuan</th>
                    <th>Harga Jual</th>
                    <th>Stok</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($barangs as $barang)
                <tr>
                    <td>{{ $barang->kd_barang}}</td>
                    <td>{{ $barang->nama_barang}}</td>
                    <td>{{ $barang->satuan }}</td>
                    <td>{{ $barang->harga_jual }}</td>
                    <td>{{ $barang->stok }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" class="text-center text-muted">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.content -->

<script>

    $( document ).ready(function() {
        $('#stokBarangDtab').DataTable({
            bLengthChange: false,
            order: [[1, 'asc'], [0, 'asc']],
            responsive: true
        });
    });
    
</script>

@endsection