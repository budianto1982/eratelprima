@extends('layouts.app-base')

@section('content-header')
<h1>Penerimaan Barang</h1>
@stop

@section('content')

<!-- Main content -->
<div class="box">
    <div class="box-body">
        <table id="pembelianDtab" class="table table-bordered table-striped dataTable" style="width:100%">
            <thead>
                <tr>
                    <th>Kode Pembelian</th>
                    <th>Tanggal Pembelian</th>
                    <th>Supplier</th>
                    <th>Total Pembelian</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pembelians as $pembelian)
                <tr>
                    <td>{{ $pembelian->kd_pembelian}}</td>
                    <td>{{ $pembelian->tgl_pembelian}}</td>
                    <td>{{ $pembelian->Supplier->nama_supplier }}</td>
                    <td>{{ $pembelian->total_pembelian }}</td>
                    <td>{{ $pembelian->status }}</td>
                    <td style="text-align: center">
                        <a href="{{ route('penerimaan.show', ['id' => $pembelian->id]) }}"
                            class="btn btn-outline-primary btn-xs" title="Detail">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a onclick="return confirm('Ubah status pembelian menjadi diterima?')"
                            href="{{ route('penerimaan.terima', ['id' => $pembelian->id]) }}"
                            class="btn btn-outline-primary btn-xs" title="Terima">
                            <i class="fa fa-check-square-o"></i>
                        </a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" class="text-center text-muted">Tidak ada data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.content -->

<script>
    $(document).ready(function () {
        $('#pembelianDtab').DataTable({
            bLengthChange: false,
            order: [
                [1, 'asc'],
                [0, 'asc']
            ],
            responsive: true
        });
    });
</script>

@endsection
