@extends('user-mgmt.base')
@section('action-content')

<div class="container">
<div class="row">
<div class="col-md-10">
<section class="panel panel-primary">
    <div class="panel-heading">Tambah User Baru</div>
    <div class="panel-body">
 
    {!! Form::open(['route' => 'user.store']) !!}

        @include('user-mgmt._user', ['roles' => $roles, 'submitLabel' => 'Save'])

    {!! Form::close() !!}
    </div>
</section>
</div>
</div>
</div>
@stop