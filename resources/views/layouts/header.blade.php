<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b></b>ER@TEL PRIMA</span>
      {{-- <span class="logo-lg"><b></b></span> --}}
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            
          
          </li>
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            {{ auth()->user()->name }}<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
              <!-- <span class="hidden-xs">Alexander Pierce</span> -->
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/LOGO EP.jpg" class="img-circle" alt="User Image">
                <!-- <li><a href="#" onclick="event.preventDefault();$('#logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li> -->

                <p>
                  Hello {{ Auth::user()->name}}
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
              @if (Auth::guest())
                <div class="pull-left">
                  <a href="{{ route('login')}}" class="btn btn-default btn-flat">Login</a>
                </div>
                @else
                <div class="pull-left">
                    <!-- <a href="{{ url('profile') }}" class="btn btn-default btn-flat">Profile</a> -->
                </div>
                <div class="pull-right">
                  <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                  Logout</a>
                 
                </div>
                @endif
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <form id="logout-form" action="{{ route('logout')}}" method="POST" style="display: none;">
  {{ csrf_Field()}}
  </form>