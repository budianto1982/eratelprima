<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('dist/img/LOGO EP.jpg') }}" alt="User Image" class="img-cirlce">
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                            class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header"></li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                </ul>
            </li>
            {{-- admin --}}
            @if (auth()->user()->role->name == "administrator")
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>CHARTS</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-circle-o text-red"></i>Profit</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>ADMIN</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('barang.index') }}"><i class="fa fa-circle-o text-aqua"></i>Master Barang</a>
                    </li>
                    <li><a href="{{ route('supplier.index') }}"><i class="fa fa-circle-o text-red"></i>Master
                            Supplier</a></li>
                    <li><a href="{{ route('pembelian.index') }}"><i class="fa fa-circle-o text-green"></i>Pembelian
                            Barang / PO</a></li>
                </ul>
            </li>
            @endif
            {{--  --}}
            @if (auth()->user()->role->name == "gudang" )
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>GUDANG</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('penerimaan.index') }}"><i class="fa fa-circle-o text-green"></i>Penerimaan Barang</a></li>
                </ul>
            </li>
            @endif
            @if (auth()->user()->role->name == "kasir")
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>KASIR</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('stock-barang.index') }}"><i class="fa fa-circle-o text-red"></i>Stok
                            Barang</a></li>
                    <li><a href="{{ route('penjualan-barang.index') }}"><i
                                class="fa fa-circle-o text-green"></i>Penjualan Barang</a></li>
                </ul>
            </li>
            @endif
            @if (auth()->user()->role->name != "gudang")
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>LAPORAN</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-circle-o text-yellow"></i>Laporan Penjualan</a></li>
                    @if (auth()->user()->role->name == "administrator" || auth()->user()->role->name == "manager")
                        <li><a href=""><i class="fa fa-circle-o text-white"></i>Laporan Pembelian</a></li>
                        <li><a href=""><i class="fa fa-circle-o text-blue"></i>Laporan Profit</a></li>
                    @endif
                </ul>
            </li>
            @endif
            @if (auth()->user()->role->name == "administrator")    
            <li>
                <a href="{{ route('user.index') }}"><i class="fa fa-users fa-fw"></i><span> User Management</span></a>
            </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
