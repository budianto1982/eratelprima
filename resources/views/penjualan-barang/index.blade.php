@extends('layouts.app-base')

@section('content-header')
<h1>Penjualan</h1>
@stop

@section('content')
<div class="box">
    <div class="box-body">
        <div style="margin-bottom: 15px">
            <div class="input-group">
                {!! Form::select('barang-dropdown', $barangs, null, ['class' => 'select', 'id' => 'barang-dropdown', 'placeholder' => 'Cari barang']) !!}
                <span class="input-group-btn">
                    <button type="button" class="btn btn-success" id="bayar-btn" style="width: 100%" data-toggle="modal" data-target="#bayar-modal" disabled>Bayar</button>
                </span>
            </div>
        </div>
        {!! Form::open(['route' => 'penjualan-barang.store', 'id' => 'penjualan-barang-form']) !!}
        <table id="item-dtab" class="table table-xs table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Harga</th>
                    <th>Stok</th>
                    <th>Jumlah</th>
                    <th>Sub Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
                <tr>
                    <td colspan="5" style="text-align:right"><strong>Total</strong></td>
                    <td>{!! Form::text('total', 0, ['class' => 'form-control', 'id' => 'total-textbox', 'readonly']) !!}</td>
                    <td>
                        {!! Form::hidden('bayar', 0, ['id' => 'bayar-textbox']) !!}
                        {!! Form::hidden('kembalian', 0, ['id' => 'kembalian-textbox']) !!}
                    </td>
                </tr>
            </tfoot>
        </table>
        {!! Form::close() !!}
    </div>
</div>

<!-- Modal -->
<div id="bayar-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pembayaran</h4>
            </div>
            <div class="modal-body">
                <div class="form-group form-focus">
                    <label class="control-label">Total</label>
                    {!! Form::text('total-modal-textbox', 0, ['class' => 'form-control', 'id' => 'total-modal-textbox', 'readonly']) !!}
                </div>
                <div class="form-group form-focus">
                    <label class="control-label">Bayar</label>
                    {!! Form::text('bayar-modal-textbox', 0, ['class' => 'form-control', 'id' => 'bayar-modal-textbox']) !!}
                </div>
                <div class="form-group form-focus">
                    <label class="control-label">Kembalian</label>
                    {!! Form::text('kembalian-modal-textbox', 0, ['class' => 'form-control', 'id' => 'kembalian-modal-textbox', 'readonly']) !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="submit-btn" disabled>Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
            </div>
        </div>

    </div>
</div>

<script>
    $(document).ready(function() {
        $('#barang-dropdown').on('select2:select', function (e) {
            var data = e.params.data;
            barangId = data.id;
            barangName = data.text;
            addItem(barangId);
        });

        $('#bayar-modal-textbox').on('keyup', function() {
            bayar = ($(this).val()) * 1;
            bayar = (bayar == '') ? 0 : bayar;
            total = ($('#total-textbox').val()) * 1;
            kembalian = bayar - total;
            $('#bayar-textbox').val(bayar);
            $('#kembalian-textbox').val(kembalian);
            $('#bayar-modal-textbox').val(bayar);
            $('#kembalian-modal-textbox').val(kembalian);

            if (bayar >= total) {
                $('#submit-btn').prop('disabled', false);
            } else {
                $('#submit-btn').prop('disabled', true);
            }
        });

        $('#submit-btn').on('click', function() {
            $('#penjualan-barang-form').submit();
        });

        function addItem(barangId) {
            $.ajax({
                url: "{{ route('penjualan-barang.tambah-barang') }}",
                type: 'GET',
                dataType: 'HTML',
                data: {
                    'barang_id': barangId
                },
                beforeSend: function() {
                    $("#barang-dropdown").prop("disabled", true);
                },
                success: function(result) {
                    $("#item-dtab tbody").append(result);
                    $('input.jumlah-textbox').keyup(function() {
                        barangId = $(this).data('barang-id');
                        jumlah = ($(this).val()) * 1;
                        calculateSubTotal(barangId, jumlah);
                    });
                    $('a.remove-item-btn').click(function() {
                        barangId = $(this).data('barang-id');
                        removeItem(barangId);
                    });
                    // Disabling selected barang option from dropdown
                    $('#barang-dropdown option[value="'+ barangId +'"]').prop('disabled', true);
                    $('.select').select2({
                        selectOnClose: true,
                        width: '100%'
                    });
                    calculateTotal();
                },
                error: function(result, status, xhr) {
                    alert(result.responseJSON.message);
                },
                complete: function() {
                    $('#barang-dropdown').val(null).trigger('change');
                    $("#barang-dropdown").prop("disabled", false);
                    checkBarangNotEmpty();
                }
            });
        }

        function removeItem(barangId) {
            $("#item-dtab tbody").find("tr#"+ barangId +"-item-dtab-tr").remove();
            // Enabling disabled barang option from dropdown
            $('#barang-dropdown option[value="'+ barangId +'"]').prop('disabled', false);
            $('.select').select2({
                selectOnClose: true,
                width: '100%'
            });
            calculateTotal();
            checkBarangNotEmpty();
        }

        function calculateSubTotal(barangId, jumlah) {
            stok = ($('#'+ barangId +'-stok-textbox').val()) * 1;
            jumlah = (jumlah == '') ? 0 : jumlah;
            
            if (jumlah > stok) {
                alert('Stok barang hanya tersedia '+ stok);
                jumlah = stok;
                $('#' + barangId + '-jumlah-textbox').val(jumlah);
            }

            harga = ($('#'+ barangId +'-harga-textbox').val()) * 1;
            subTotalBarang = harga * jumlah;
            $('#'+ barangId +'-sub-total-textbox').val(subTotalBarang);
            calculateTotal();
        }

        function calculateTotal() {
            total = 0;
            $('input.sub-total-textbox').each(function( index ) {
                subTotal = ($(this).val()) * 1;
                total = total + subTotal;
            });
            $('#total-textbox').val(total);
            $('#total-modal-textbox').val(total);
            $('#bayar-textbox').val(0);
            $('#kembalian-textbox').val(0);
            $('#bayar-modal-textbox').val(0);
            $('#kembalian-modal-textbox').val(0);
        }

        function checkBarangNotEmpty() {
            if ($('#item-dtab tbody tr').length) {
                $('#bayar-btn').prop('disabled', false);
            } else {
                $('#bayar-btn').prop('disabled', true);
            }
        }
    });
</script>

@stop