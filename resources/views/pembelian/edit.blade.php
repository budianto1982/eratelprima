@extends('pembelian.base')
@section('action-content')

<div class="panel panel-primary">
    <div class="panel-heading">Edit Data Pembelian Barang</div>
    <div class="panel-body">

    {!! Form::model($pembelian, ['route' => ['pembelian.update', $pembelian->id], 'method' => 'PATCH']) !!}

        @include('pembelian._pembelian', ['roles' => $roles, 'submitLabel' => 'Update'])

    {!! Form::close() !!}

    
    </div>
</div>

@stop