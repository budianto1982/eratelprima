<div class="form-group">
    {!! Form::hidden('id', null) !!}
    {!! Form::hidden('kd_supplier', null) !!}
    {!! Form::label('nama_supplier', 'Nama Supplier') !!}
    {!! Form::text('nama_supplier', null, ['class' => 'form-control', 'required']) !!}
</div>
<div class="form-group">
    {!! Form::label('alamat', 'Alamat') !!}
    {!! Form::textarea('alamat',  null, ['class' => 'form-control', 'rows' => 2, 'required']) !!}
</div>
<div class="form-group">
    {!! Form::label('no_telp', 'No. Telp') !!}
    {!! Form::text('no_telp',  null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('email', 'Email') !!}
    {!! Form::email('email',  null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitLabel, ['class' => 'btn btn-success pull-right']) !!}
    <a onclick="window.history.back()" type="button" class="btn btn-default pull-right">Cancel</a>
</div>