<div class="form-group">
    {!! Form::label('supplier_id', 'Supplier') !!}
    {!! Form::select('supplier_id', $suppliers, null, ['class' => 'select form-control', 'id' => 'supplier-dropdown']) !!}
</div>
<div class="form-group">
    {!! Form::label('nama_barang', 'Nama Barang') !!}
    {!! Form::text('nama_barang', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('satuan', 'Satuan') !!}
    {!! Form::text('satuan',  null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('harga_jual', 'Harga Jual') !!}
    {!! Form::text('harga_jual', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('harga_beli', 'Harga Beli') !!}
    {!! Form::text('harga_beli', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('stok', 'Stok') !!}
    {!! Form::text('stok', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit($submitLabel, ['class' => 'btn btn-success pull-right']) !!}
    <a onclick="window.history.back()" type="button" class="btn btn-default pull-right">Cancel</a>
</div>