<?php

namespace App\Http\Controllers;
use App\barang;
use App\Supplier;
use App\Pembelian;
use App\PembelianDetail;
use Illuminate\Http\Request;
use DB;

class PembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $suppliers = Supplier::orderBy('nama_supplier')->pluck('nama_supplier', 'id')->all();
        $pembelians = Pembelian::with('Supplier')->orderBy('tgl_pembelian', 'desc')->get();

        //  return $barangs;
         return view('pembelian.index', compact('pembelians', 'suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $supplierId = (isset($request->supplier_id)) ? $request->supplier_id : '';

        if ($supplierId == '') {
            return redirect()->route('pembelian.index')->with('flash_notification_message', 'Mohon untuk memilih supplier terlebih dahulu');
        } else {
            $supplier = Supplier::find($supplierId);

            if ($supplier) {
                $model = new Pembelian;
                $model->supplier_id = $supplierId;
                $barangs = Barang::where('supplier_id', $supplierId)
                    ->orderBy('nama_barang')
                    ->pluck('nama_barang', 'id')
                    ->all();

                return view('pembelian.create', compact('model', 'supplier', 'barangs'));
            } else {
                return redirect()->route('pembelian.index')->with('flash_notification_message', 'Supplier tidak ditemukan');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tambahBarang(Request $request)
    {
        $barangId = $request->barang_id;
        $barang = barang::find($barangId);
        $defaultJumlah = 1;

        return view('pembelian._pembelian', compact('barang', 'defaultJumlah'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->barang_ids, $request->kd_barangs, $request->nama_barangs)) {
            try {
                $kdPembelian = $this->getLatestKode();
                $tglPembelian = date('Y-m-d');
                $supplierId = $request->supplier_id;
                $total = $request->total_pembelian;
                $total = ($total != '' && $total != 0) ? $total : array_sum($request->sub_totals);
                $status = "dipesan";

                // Create Pembelian
                DB::beginTransaction();
                $pembelian = new Pembelian;
                $pembelian->kd_pembelian = $kdPembelian;
                $pembelian->tgl_pembelian = $tglPembelian;
                $pembelian->supplier_id = $supplierId;
                $pembelian->total_pembelian = $total;
                $pembelian->status = $status;
                $pembelian->pembuat_user_id = \Auth::user()->id;
                
                // Save Pembelian
                if ($pembelian->save()) {
                    // Create Pembelian Detail
                    $pembelianDetails = [];
                    $barangIds = $request->barang_ids;
                    $kodeBarangs = $request->kd_barangs;
                    $namaBarangs = $request->nama_barangs;
                    $hargaBarangs = $request->harga_belis;
                    $jumlahs = $request->jumlahs;
                    $subTotals = $request->sub_totals;
                    for ($i=0; $i < count($barangIds); $i++) {
                        $pembelianDetail = new pembelianDetail;
                        $pembelianDetail->pembelian_id = $pembelian->id;
                        $pembelianDetail->barang_id = $barangIds[$i];
                        $pembelianDetail->kd_barang = $kodeBarangs[$i];
                        $pembelianDetail->nm_barang = $namaBarangs[$i];
                        $pembelianDetail->harga_beli = $hargaBarangs[$i];
                        $pembelianDetail->jumlah = $jumlahs[$i];
                        $pembelianDetail->sub_total = $subTotals[$i];
                        $pembelianDetail->save();
                    }
                    DB::commit();
                }
            } catch (Exception $e) {
                DB::rollback();
                $httpCode = $e->getCode();
                $message = $e->getMessage();
            }
        } else {
            $httpCode = 404;
            $message = "Mohon untuk menginput data barang yang akan dibeli terlebih dahulu";
        }

        return redirect()->route('pembelian.index')->with('flash_notification_message', 'Pembelian berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $pembelian = Pembelian::find($id);
        $pembelianDetails = PembelianDetail::where('pembelian_id', $id)->orderBy('kd_barang', 'asc')->get();
        $currRouteName = $request->route()->getName();
        $showTerimaButton = ($currRouteName == 'penerimaan.show') ? true : false;

        return view('pembelian.show', compact('pembelian', 'pembelianDetails', 'showTerimaButton'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pembelian = Pembelian::find($id);

        if ($pembelian) {
            $pembelian = Pembelian::where('id', $id)->delete();
            $pembelianDetail = PembelianDetail::where('pembelian_id', $id)->delete();
        } else {
            return abort(404);
        }

        return redirect()->route('pembelian.index')->with('flash_notification_message', 'Pembelian berhasil dihapus');
    }

    private function getLatestKode() {
        $latestKode = "PO";
        $rowExists = Pembelian::whereNotNull('kd_pembelian')->count();
        if ($rowExists > 0) {
            $latestKode .= 
                collect(
                    DB::select(
                        "SELECT LPAD(CAST(RIGHT(kd_pembelian, 5) AS UNSIGNED) + 1, 5, '0') AS kd_pembelian FROM pembelian ORDER BY CAST(RIGHT(kd_pembelian, 5) AS UNSIGNED) DESC LIMIT 1"
                    )
                )->first()->kd_pembelian;
        } else {
            $latestKode .= "00001";
        }

        return $latestKode;
    }

    /**
     * Display a listing of the ordered resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function penerimaan(Request $request)
    {
        $pembelians = Pembelian::with('Supplier')
            ->where('status', 'DIPESAN')
            ->orderBy('tgl_pembelian', 'asc')
            ->get();

        //  return $barangs;
         return view('pembelian.penerimaan', compact('pembelians'));
    }

    /**
     * Display a listing of the ordered resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function terima(Request $request, $id)
    {
        $pembelian = Pembelian::find($id);
        $status = $pembelian->status;

        // Check whether given pembelian data is already "DITERIMA" or something else
        if ($status != "DIPESAN") {
            return redirect()->route('penerimaan.index')
                ->with('flash_notification_message',
                    'Proses update status gagal. Status pembelian barang dengan kode '. $pembelian->kd_pembelian .' sudah diterima sebelumnya!');
        }

        // Update the status into "DITERIMA"
        $pembelian->status = "DITERIMA";
        if ($pembelian->save()) {
            // Update stok barang if pembelian is updated successfully
            $pembelianId = $pembelian->id;
            $pembelianDetails = PembelianDetail::where('pembelian_id', $pembelianId)->get();
            if ($pembelianDetails->count() > 0) {
                foreach ($pembelianDetails as $detail) {
                    $barangId = $detail->barang_id;
                    $stokTambahan = $detail->jumlah;

                    // Update stok
                    $barang = barang::find($barangId);
                    $stokSekarang = $barang->stok;
                    $stokSekarang = ($stokSekarang == null || $stokSekarang == '') ? 0 : $stokSekarang;
                    $stok = $stokSekarang + $stokTambahan;
                    $barang->stok = $stok;
                    $barang->save();
                }
            }
            
            return redirect()->route('penerimaan.index')->with('flash_notification_message',
                'Status pembelian barang dengan kode '. $pembelian->kd_pembelian .' berhasil diupdate');
        }

        return redirect()->route('penerimaan.index')->with('flash_notification_message', 'Terjadi masalah, hubungi sistem administrator');
    }
}
