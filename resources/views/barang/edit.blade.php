@extends('layouts.app-base')

@section('content-header')
<h1>Master Barang <small>Ubah Data</small></h1>
@stop

@section('content')

<!-- Main content -->
<div class="box">
    <div class="box-body">
        {!! Form::model($barang, ['route' => ['barang.update', $barang->id], 'method' => 'PATCH', 'style' => 'padding:15px']) !!}
        @include('barang._barang', ['submitLabel' => 'Update'])
        {!! Form::close() !!}
    </div>
</div>

@stop